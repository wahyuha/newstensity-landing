import React from 'react';
import styles from './styles.scss';

class ContactUs {
  static components(props) {
    return new ContactUs().root() ;
  }

  root() {
    return (
      <div className={styles.contact_us}>
        <div className={styles.contact_us_wrap}>
          <div className={styles.contact_info}>
            <h1>Contact Us</h1>
            <div className={styles.contact_p}>
              <h2>PT. Binokular Media Utama</h2>
              <p>Started career as journalist since 1999, TBS - his famously known nickname, is full with editorial experience in almost all areas of reportage. Started from Entertainment news, Political events, Sports, Economy and Information Technology. At the end of his career at Detik.com, he was trusted to handle their content development business core. Then, he started a new challenge to initiate Binokular until now On 2016, he was became co-founder of Tirto.ID</p>
            </div>
          </div>
          <div className={styles.contact_form}>
            <h2>Ask for Trial</h2>
            <div className={styles.form_section}>
              <label htmlFor="">Name*</label>
              <input type="text" placeholder={'Your Name'}/>
            </div>
            <div className={styles.form_section}>
              <label htmlFor="">Email*</label>
              <input type="text" placeholder={'Email Address'}/>
            </div>
            <div className={styles.form_section}>
              <label htmlFor="">Phone Number*</label>
              <input type="text" placeholder={'+62 000000'}/>
            </div>
            <div className={styles.form_section}>
              <label htmlFor="">Company / Location</label>
              <input type="text" placeholder={'Company Name / City'}/>
            </div>
            <div className={styles.form_section}>
              <label htmlFor="">Message</label>
              <textarea name="" cols="30" rows="7" placeholder={'Any further question?'}></textarea>
            </div>
            <div className={styles.form_section}>
              <button>Submit</button>
            </div>
          </div>
        </div>
        <div className={styles.footer_text}>Newstensity © 2019</div>
      </div>
    );
  }
}

export default ContactUs.components;
