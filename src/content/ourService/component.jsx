import React from 'react';
import classNames from 'classnames';
import { Doughnut } from 'react-chartjs-2';
import donutProps from './donut.json';
import Jkw from './assets/jkw.jpg';
import Prabowo from './assets/prabowo.jpg';
import styles from './styles.scss';

class OurService {
  static components(props) {
    return new OurService().root() ;
  }

  root() {
    return (
      <div className={styles.our_service}>
        <div className={styles.service_content_wrap}>
          <div className={styles.service_content}>
            <h3>According to Press Council data, there are 47.000 {'\n'} mass media in Indonesia*</h3>
            <ul>
              <li>93% is Online Media, 4% is Printed Media, the 		rest 2% is Electronic Media*</li>
              <li>Don’t forget to count the comments from 		social media</li>
              <li>Do you able to gathered data and compile 		those contents?</li>
              <li>Could you pull connecting line across 		mainstream and social media?</li>
              <li>Which person and entities related to you the 		most?</li>
            </ul>
          </div>
        </div>
        {this.searchService()}
      </div>
    );
  }

  searchService() {
    return (
      <div className={styles.service_search}>
        <div className={styles.form_control}>
          <input type="text" />
          <button>Search</button>
        </div>
        {this.searchTopic()}
        {this.searchBorder('Result')}
        {this.searchResult()}
        {this.searchBorder('Watch the Global Trend')}
        {this.globalTrend()}
      </div>
    );
  }

  searchTopic() {
    return (
      <div className={styles.search_topic}>
        <h4>Active Topic</h4>
        <div className={styles.topics_wrap}>
          <div className={styles.topic_item}>
            <div>Pemilihan Presiden 2019</div>
            <button />
          </div>
          <div className={styles.topic_item}>
            <div>Pemilihan Presiden 2019</div>
            <button />
          </div>
        </div>
      </div>
    );
  }

  searchBorder(title) {
    return (
      <div class="border_wrap">
        <span>{title}</span>
        <div class="border_line"></div>
      </div>
    );
  }

  searchResult() {
    return (
      <div className={styles.result}>
        <div className={styles.result_graph}>
          <h4>Top Person</h4>
          {this.row1()}
          {this.row2()}
        </div>
        {this.mostRelated()}
      </div>
    );
  }

  row1() {
    return (
      <div className={styles.result_row}>
        {this.graph(Jkw)}
        <div className={styles.row_info}>
          <h3>Joko Widodo & Maruf Amin</h3>
          <div className={styles.graph_text}>Online Media</div>
          <div className={classNames(styles.graph_label, styles.bg_blue)} />
          <div className={styles.graph_text}>Social Media</div>
          <div className={classNames(styles.graph_label, styles.bg_green)} />
        </div>
      </div>
    );
  }

  row2() {
    return (
      <div className={styles.result_row}>
        {this.graph(Prabowo)}
        <div className={styles.row_info}>
          <h3>Prabowo & Sandiaga Uno</h3>
          <div className={styles.graph_text}>Online Media</div>
          <div className={classNames(styles.graph_label, styles.bg_blue)} />
          <div className={styles.graph_text}>Social Media</div>
          <div className={classNames(styles.graph_label, styles.bg_green)} />
        </div>
      </div>
    );
  }

  graph(img_file) {
    return (
      <div className={styles.graph_wrapper}>
        <div className={styles.graph_cards}>
          <Doughnut {...donutProps} />
          <div className={styles.profile_circle}>
            <img src={img_file} />
          </div>
        </div>
      </div>
    );
  }

  mostRelated() {
    return (
      <div className={styles.result_list}>
        <h4>Most Related News</h4>
        <div className={styles.list_item}>
          <h4><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></h4>
          <div className={styles.label}>Positive</div>
          <div className={styles.soft_item}>mediaindonesia.com</div>
          <div>November 15, 2018 - 22:15:00</div>
        </div>
        <div className={styles.list_item}>
          <h4><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></h4>
          <div className={styles.label}>Positive</div>
          <div className={styles.soft_item}>mediaindonesia.com</div>
          <div>November 15, 2018 - 22:15:00</div>
        </div>
        <div className={styles.list_item}>
          <h4><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></h4>
          <div className={styles.label}>Positive</div>
          <div className={styles.soft_item}>mediaindonesia.com</div>
          <div>November 15, 2018 - 22:15:00</div>
        </div>
      </div>
    );
  }

  globalTrend() {
    return (
      <div className={styles.global_trend}>
        {this.globalItem('Hot Topics')}
        {this.globalItem('Top 10 News')}
        {this.globalItem('Most Popular Brand')}
        <div className={styles.see_all}>
          <div className={styles.blocking}><a href="#">You may request for trial to see all widgets!</a></div>
        </div>
      </div>
    );
  }

  globalItem(title) {
    return (
      <div className={styles.global_item}>
        <h4>{title}</h4>
        <ul>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
          <li><a href="#">Tahun Politik, IKAPMII Ajak Masyarakat Perangi Hoaks</a></li>
        </ul>
      </div>
    );
  }
}

export default OurService.components;
