import React from 'react';
import Logo from './assets/logo.svg';
import scrollToElement from 'scroll-to-element';
import styles from './styles.scss';

class Headerbar {
  static components(props) {
    return (
      <div className={styles.header}>
        <div className={styles.logo}>
          <a><img src={Logo} /></a>
        </div>
        <ul className={styles.main_menu}>
          <li><a onClick={() => scrollToElement('.about_us', {offset: 0, ease: 'out-circ', duration: 500 })}>About Us</a></li>
          <li><a onClick={() => scrollToElement('.our_service', {offset: 0, ease: 'out-circ', duration: 1000 })}>Our Service & Solutions</a></li>
          <li><a onClick={() => scrollToElement('.why_us', {offset: 0, ease: 'out-circ', duration: 1000 })}>Why Us</a></li>
          <li><a onClick={() => scrollToElement('.our_board', {offset: 0, ease: 'out-circ', duration: 1000 })}>Our Boards</a></li>
          <li><a onClick={() => scrollToElement('.pricing', {offset: 0, ease: 'out-circ', duration: 1000 })}>Pricing</a></li>
          <li><a onClick={() => scrollToElement('.contact_us', {offset: 0, ease: 'out-circ', duration: 1000 })}>Contact Us</a></li>
        </ul>
        <ul className={styles.login}>
          <li><a>Login</a></li>
        </ul>
      </div>
    );
  }
}

export default Headerbar.components;
