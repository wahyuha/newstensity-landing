import React from 'react';
import classNames from 'classnames';
import styles from './styles.scss';

class Pricing {
  static components(props) {
    return new Pricing().root() ;
  }

  root() {
    return (
      <div className={styles.pricing}>
        <h1>Pricing & Specs</h1>
        <div className={styles.pricing_wrap}>
          {this.pricingRowHeader()}
          {this.pricingRow(['Media Online', 'All Media Online', 'All Media Online', 'All Media Online'], true)}
          {this.pricingRow([
            'Social Media', 
            'All Scoal Media \n Twitter, Facebook, Instagram, Youtube, Forum',
            'All Scoal Media \n Twitter, Facebook, Instagram, Youtube, Forum', 
            'All Scoal Media \n Twitter, Facebook, Instagram, Youtube, Forum'
          ])}
          {this.pricingRow([
            'Media Online Comment Section', 
            'Yes',
            'No', 
            'Yes'
          ], true)}
          {this.pricingRow([
            'Sentiment Analysis', 
            'All Sentiment : \n Media Sentiment & Public Sentiment',
            'All Sentiment : \n Media Sentiment & Public Sentiment',
            'All Scoal Media \n Twitter, Facebook, Instagram, Youtube, Forum'
          ])}
          {this.pricingRow([
            'Tracking Period', 
            '2013 Onward',
            '2018 Onward', 
            '2013 Onward'
          ], true)}
          {this.pricingRow([
            'Keyword', 
            'Unlimited',
            'Up to 30 Keywords \n Up to 5 Keyword Changes per Week', 
            'Unlimited'
          ])}
          {this.pricingRow([
            'Data Archive', 
            'Unlimited Data Archive',
            'Unlimited Data Archive', 
            'Unlimited Data Archive'
          ], true)}
          {this.pricingRow([
            'User Account', 
            'Max 10 User',
            'Unlimited User', 
            'Unlimited User'
          ])}
          {this.pricingRow([
            'Admin Account', 
            '1 Admin Account',
            '10 Admin Account', 
            'Unlimited Admin Account'
          ], true)}
          {this.pricingRow([
            'PDF Version', 
            'Yes',
            'Yes', 
            'Yes'
          ])}
          {this.pricingRow([
            'Topic', 
            'All Topic / Industry',
            'Clustering Relevant Issue in Client Industry', 
            'Clustering Relevant Issue in Client Industry'
          ], true)}
          {this.pricingRow([
            'PPT File Report', 
            'Yes',
            'Yes', 
            'Yes'
          ])}
          {this.pricingRow([
            'Excel File Report', 
            'Yes',
            'Yes', 
            'Yes'
          ], true)}
          {this.pricingRow([
            'Tracking Issue Initiator', 
            'Yes',
            'Yes', 
            'Yes'
          ])}
          {this.pricingRow([
            'Ontology', 
            'Yes',
            'Yes', 
            'Yes'
          ], true)}
          {this.pricingRow([
            'Additional Media', 
            '500 Printed Media \n 19 Tv \n Chargable',
            'Selected Printed Media \n 19 Tv \n Chargable', 
            'Selected Printed Media \n 19 Tv \n Chargable'
          ])}
          {this.pricingRow([
            'Additional Year', 
            'Chargable',
            'Free', 
            'Free'
          ], true)}
          {this.pricingRowFooter()}
        </div>
      </div>
    );
  }

  pricingRow(cols, isGrey) {
    const rowClass = classNames({
      [styles.pricing_row]: true,
      [styles.row_grey]: isGrey
    });
    return (
      <div className={rowClass}>
        <div className={styles.pricing_column}>{cols[0] || ''}</div>
        <div className={styles.pricing_column}>{cols[1]}</div>
        <div className={styles.pricing_column}>{cols[2]}</div>
        <div className={styles.pricing_column}>{cols[3]}</div>
      </div>
    );
  }
  
  pricingRowHeader() {
    return (
      <div className={styles.pricing_row}>
        <div className={classNames(styles.column_header, styles.header_empty)}></div>
        <div className={styles.column_header}>
          <div>
            <h3>Holding</h3>
            <div>Great for Holding Company</div>
          </div>
        </div>
        <div className={styles.column_header}>
          <div>
            <h3>Coorporate</h3>
            <div>Good for Corporate and Brand Analysis</div>
          </div>
        </div>
        <div className={styles.column_header}>
          <div>
            <h3>Advanced</h3>
            <div>Great for Holding Company</div>
          </div>
        </div>
      </div>
    );
  }

  pricingRowFooter(cols) {
    return (
      <div className={styles.pricing_row}>
        <div className={classNames(styles.column_footer, styles.footer_empty)}></div>
        <div className={styles.column_footer}>
          <button>Contact Us</button>
        </div>
        <div className={styles.column_footer}>
          <button>Contact Us</button>
        </div>
        <div className={styles.column_footer}>
          <button>Contact Us</button>
        </div>
      </div>
    );
  }

  pricingColumn() {
    return (
      <div className={styles.pricing_column}>
        <div className={classNames(styles.pricing_item, styles.pricing_heading)}>
          <h3>Holding</h3>
          <div>Great for Holding Company</div>
        </div>
        <div className={styles.pricing_item_grey}>All Media Online</div>
        <div className={styles.pricing_item}>
          <div>
            Okeee
          </div>
          All Scoal Media : {'\n'}
          Twitter, Facebook, Instagram, Youtube, Forum
        </div>
        <div className={styles.pricing_item_grey}>Yes</div>
        <div className={styles.pricing_item}>
          All Sentiment : {'\n'}          
          Media Sentiment &
          Public Sentiment
        </div>
        <div className={styles.pricing_item_grey}>Yes</div>
        <div className={styles.pricing_item}>
          All Sentiment : {'\n'}          
          Media Sentiment &
          Public Sentiment
        </div>
        <div className={styles.pricing_item_grey}>All Media Online</div>
        <div className={styles.pricing_item}>
          All Scoal Media : {'\n'}
          Twitter, Facebook, Instagram, Youtube, Forum
        </div>
        <div className={styles.pricing_item_grey}>Yes</div>
        <div className={styles.pricing_item}>
          All Sentiment : {'\n'}          
          Media Sentiment &
          Public Sentiment
        </div>
        <div className={classNames(styles.pricing_item, styles.pricing_heading, styles.pricing_footer)}>
          <button>Contact Us</button>
        </div>
      </div>
    );
  }
}

export default Pricing.components;
