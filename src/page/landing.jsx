import React from 'react';
import AboutUs from '../content/aboutUs/component.jsx';
import ContactUs from '../content/contactUs/component.jsx';
import HeaderBar from '../content/headerBar/component.jsx';
import OurBoard from '../content/ourBoard/component.jsx';
import OurService from '../content/ourService/component.jsx';
import Pricing from '../content/pricing/component.jsx';
import WhyUs from '../content/whyUs/component.jsx';
import Wrapper from '../content/wrapper/component.jsx';

export default class MediaPage extends React.Component {
  render() {
    return (
      <Wrapper>
        <HeaderBar />
        <AboutUs />
        <OurService />
        <WhyUs />
        <Pricing />
        <OurBoard />
        <ContactUs />
      </Wrapper>
    );
  }
}